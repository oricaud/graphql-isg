const fetch = require("node-fetch");

export class Policy {
    constructor() {
        this.baseUrl = "https://ci2uljipkj.execute-api.us-east-1.amazonaws.com";
        this.version = "/v1/";
        this.businessGroup = "/isg/";
    }

    fetch() {
        return fetch(this.baseUrl + this.version + this.businessGroup + 'policies/')
            .then(function(response) {
                return response.json();
            })
            .then(function(myJson) {
                console.log('myJson policy ' + JSON.stringify(myJson));
                return myJson;
            })
    }
}


