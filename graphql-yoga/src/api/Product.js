const fetch = require("node-fetch");
const request = require('request');

/**
 * Class is responsible for handling CRUD operations for the Dynamo DB Product Table
 */
export class Product {

    constructor() {
        this.baseUrl = "https://yjn03pce5i.execute-api.us-east-1.amazonaws.com/";
        this.version = "v5/";
        this.businessGroup = "isg/";
        this.resource = "products/";
    }

    /**
     * Method is responsible for adding a product to the Dynamo DB Product Table
     * @param product is the product that we are sending to the API
     */
    add(product) {
        console.log('adding product ' + JSON.stringify(product));
        request.post({
                headers: {'content-type' : 'application/json'},
                method: 'POST',
                url: this.baseUrl + this.version + this.businessGroup + this.resource,
                json: product,
            },
            function (error, response, body) {
                console.log('BODY ' + JSON.stringify(body));
                console.log('RESPONSE ' + JSON.stringify(response));
                console.log('ERROR ' + JSON.stringify(error));
                return body;
            }
        );
    }

    /**
     * Method is responsible for fetching resource /product from the AWS API Gateway
     * @returns {*} myJson which is the list of products if any
     */
    fetch() {
        return fetch(this.baseUrl + this.version + this.businessGroup + this.resource)
            .then(function(response) {
                return response.json();
            })
            .then(function(myJson) {
                console.log('myJson ' + JSON.stringify(myJson));
                return myJson;
            });
    }

    /**
     * Method is responsible for deleting a row from the Product Table given an ID
     * @param product is the object we are removing from the database
     */
    remove(product) {
        console.log('removing product id ' + product.ID);
        request.delete({
                headers: {'content-type': 'application/json'},
                method: 'DELETE',
                url: this.baseUrl + this.version + this.businessGroup + this.resource,
                json: product
            },
            function (error, response, body) {
                console.log('BODY ' + JSON.stringify(body));
                console.log('RESPONSE ' + JSON.stringify(response));
                console.log('ERROR ' + JSON.stringify(error));
            }
        );
    }

}

