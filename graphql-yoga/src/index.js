import {resolvers} from "./resolvers/product-resolver"

const {GraphQLServer} = require('graphql-yoga');

const server = new GraphQLServer({
    typeDefs: './schema/typeDefs.graphql',
    resolvers: resolvers
});

server.start(() => console.log(`Server is running on http://localhost:4000`));