import {Product} from "../api/Product";
import {Policy} from "../api/Policy";

let myProduct = new Product();
let myPolicy = new Policy();

export const resolvers = {
    Query: {
        policy: () => myPolicy.fetch(),
        product: () => myProduct.fetch(),
    },
    Mutation: {
        deleteProduct: (parent, args) => {
            const product = {
                ID: args.ID,
            };
            myProduct.remove(product);
        },
        postProduct: (parent, args) => {
            const product = {
                ID: Math.floor((Math.random() * 1000) + 1).toString(),
                name: args.name,
                effective: args.effective,
                start: args.start,
                end: args.end,
            };
            myProduct.add(product);
            return product
        },
        updateProduct: (parent, args) => {
            const link = {
                description: args.description,
                url: args.url
            };
            links.forEach(function (x) {
                if (x.id === args.id) {
                    x.id = args.id;
                    x.description = args.description;
                    x.url = args.url;
                }
            });
            return link;
        },
    },
};
