import React, { Component } from 'react'
import Product from './Product'
import gql from 'graphql-tag';
import { Query } from 'react-apollo'

const FEED_QUERY = gql`    
    {
        product {
            name
            start
            end
            effective
        }
    }
`;

class ProductList extends Component {
    render() {
        return (
            <Query query={FEED_QUERY}>
                {({ loading, error, data }) => {
                    if (loading)
                        return <div>Fetching</div>;

                    if (error)
                        return <div>Error</div>;

                    const linksToRender = data.product;

                    return (
                        <div>
                            {linksToRender.map(link => <Product key={link.id} link={link} />)}
                        </div>
                    )
                }}
            </Query>
        )
    }
}

export default ProductList