import React, { Component } from 'react'

class Product extends Component {
    render() {
        return (
            <div>
                <div>
                    Name: {this.props.link.name}
                </div>
                {this.props.link.start}
                {this.props.link.end}
            </div>
        )
    }
}

export default Product