import React, { Component } from 'react'
import ProductList from './ProductList'

class App extends Component {
  render() {
    return <ProductList />
  }
}

export default App